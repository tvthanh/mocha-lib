//
//  StringeeCallManager.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeCallFactory.h"
#import "StringeeCall.h"


@implementation StringeeCallFactory

static StringeeCallFactory *sharedStringeeCallManager = nil;

+ (StringeeCallFactory*)instance {
    @synchronized(self) {
        if (sharedStringeeCallManager == nil)
            sharedStringeeCallManager = [[self alloc] init];
        
    }
    
    return sharedStringeeCallManager;
}

- (id)init {
    self = [super init];
    if (self) {
        self.preferredAudioCodec = @"";
        self.preferredAudioCodec = @"";
    }
    return self;
}

- (void)initSdk:(BOOL)loudspeaker {

    
//    NSLog(@"initSdk");
    
    //init
    
//    NSDictionary *fieldTrials = @{
//                                  kRTCFieldTrialImprovedBitrateEstimateKey: kRTCFieldTrialEnabledValue,
//                                  kRTCFieldTrialH264HighProfileKey: kRTCFieldTrialEnabledValue,
//                                  };
    
    NSDictionary *fieldTrials = @{
                                  kRTCFieldTrialImprovedBitrateEstimateKey: kRTCFieldTrialEnabledValue,
                                  kRTCFieldTrialH264HighProfileKey: kRTCFieldTrialEnabledValue,
                                  kRTCFieldTrialSendSideBweWithOverheadKey: kRTCFieldTrialEnabledValue,
                                  kRTCFieldTrialAudioSendSideBweKey: kRTCFieldTrialEnabledValue
                                  };
    RTCInitFieldTrialDictionary(fieldTrials);
    
    
//    RTCInitializeSSL();
//    RTCSetupInternalTracer();
    
//    RTCInitFieldTrials(kRTCFieldTrialAudioSendSideBweKey);
    RTCInitializeSSL();
//    RTCSetupInternalTracer();
    
    //Audio Session config
    RTCAudioSessionConfiguration *webRTCConfig = [RTCAudioSessionConfiguration webRTCConfiguration];
    if (loudspeaker) {
        webRTCConfig.categoryOptions = webRTCConfig.categoryOptions | AVAudioSessionCategoryOptionDefaultToSpeaker;
    }else{
        webRTCConfig.categoryOptions = webRTCConfig.categoryOptions & ~AVAudioSessionCategoryOptionDefaultToSpeaker;
    }
    
//    NSLog(@"webRTCConfig.mode: %@", webRTCConfig.mode);
    
    [RTCAudioSessionConfiguration setWebRTCConfiguration:webRTCConfig];
    
    //create PC Factory
    self.factory = [[RTCPeerConnectionFactory alloc] init];
    
    RTCAudioSession *session = [RTCAudioSession sharedInstance];
    session.useManualAudio = YES;
    session.isAudioEnabled = NO;
    [session addDelegate:self];
}

- (void)cleanupSdk {
    self.factory = nil;
    RTCCleanupSSL();
//    RTCShutdownInternalTracer();
}

- (StringeeCall*)createCall {
    return [[StringeeCall alloc] initWithFactory:self];
}

+(void) setupUseManualAudio:(BOOL) state {
    
    RTCAudioSession *session = [RTCAudioSession sharedInstance];
    session.useManualAudio = state;
    
}

+(void) setAudioEnabled:(BOOL) state {
    RTCAudioSession *session = [RTCAudioSession sharedInstance];
    session.isAudioEnabled = state;
    
}


@end
