//
//  StringeeIceServer.h
//  StringeeWebRtcLib
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringeeIceServer : NSObject

@property(nonatomic, readonly) NSArray<NSString *> *urlStrings;

@property(nonatomic, readonly) NSString *username;

@property(nonatomic, readonly) NSString *credential;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithURLStrings:(NSArray<NSString *> *)urlStrings;

- (instancetype)initWithURLStrings:(NSArray<NSString *> *)urlStrings
                          username:(NSString *)username
                        credential:(NSString *)credential;

@end
