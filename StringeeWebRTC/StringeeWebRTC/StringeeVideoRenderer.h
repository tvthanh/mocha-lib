//
//  StringeeVideoRenderer.h
//  TestWebrtcnew
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRTC.h"
#import "StringeeVideoFrame.h"

@protocol StringeeVideoRenderer <RTCVideoRenderer>

- (void)setSize:(CGSize)size;

- (void)renderFrame:(nullable StringeeVideoFrame *)frame;

@end
