//
//  SessionDescriptionDelegate.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRTC.h"

@class StringeeCall;

@interface SessionDescriptionDelegate : NSObject {
    BOOL isCaller;
}

@property (weak, nonatomic) StringeeCall* call;


- (SessionDescriptionDelegate*)initAsCaller:(BOOL)isCaller1 call:(StringeeCall*)call1;

- (void)didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error isVideoCall:(BOOL)isVideoCall;

- (void)didSetLocalSDPCallBack:(NSError *)error local:(BOOL)setLocalSdp offer:(BOOL)setOfferSdp isVideoCall:(BOOL)isVideoCall;
- (void)didSetRemoteSDPCallBack:(NSError *)error local:(BOOL)setLocalSdp offer:(BOOL)setOfferSdp isVideoCall:(BOOL)isVideoCall;

@end
