//
//  StringeeIceCandidate.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringeeIceCandidate : NSObject

@property(nonatomic, strong) NSString *sdpMid;

@property(nonatomic, assign) int sdpMLineIndex;

@property(nonatomic, strong) NSString *sdp;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithSdp:(NSString *)sdp sdpMLineIndex:(int)sdpMLineIndex sdpMid:(NSString *)sdpMid;

@end
