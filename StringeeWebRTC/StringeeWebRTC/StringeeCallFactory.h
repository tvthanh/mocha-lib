//
//  StringeeCallManager.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WebRTC.h"
#import "RTCAudioSessionConfiguration.h"
#import "RTCAudioSession.h"

#import "PeerConnectionDelegate.h"
#import "SessionDescriptionDelegate.h"

#import "StringeeIceCandidate.h"

@class StringeeCall;

@interface StringeeCallFactory : NSObject <RTCAudioSessionDelegate>


@property (strong, nonatomic) RTCPeerConnectionFactory* factory;
@property (strong, nonatomic) NSString * preferredVideoCodec;
@property (strong, nonatomic) NSString * preferredAudioCodec;

+ (StringeeCallFactory*)instance;

- (void)initSdk:(BOOL)defaultLoudspeaker;
- (void)cleanupSdk;

- (StringeeCall*)createCall;

-(void) setPreferredVideoCodec:(NSString *)preferredVideoCodec;
-(void) setPreferredAudioCodec:(NSString *)preferredAudioCodec;
+(void) setupUseManualAudio:(BOOL) state;
+(void) setAudioEnabled:(BOOL) state;

@end
