//
//  SdpUtil.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 6/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RTCLogging.h"
#import "RTCSessionDescription.h"

@interface SdpUtil : NSObject

+ (RTCSessionDescription *)descriptionForDescription:(RTCSessionDescription *)description preferredVideoCodec:(NSString *)codec;

+ (RTCSessionDescription *)descriptionForDescription:(RTCSessionDescription *)description preferredAudioCodec:(NSString *)codec;

@end
