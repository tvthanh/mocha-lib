//
//  Chart.m
//  StringeeWebRTC
//
//  Created by Nguyen Huu Tien on 5/31/19.
//  Copyright © 2019 Dau Ngoc Huy. All rights reserved.
//

#import "Chart.h"

@implementation Chart
-(instancetype)initWithDictionary:(NSDictionary *)infoDict
{
    self.startTime = infoDict[@"startTime"] ? infoDict[@"startTime"] : @"";
    self.endTime = infoDict[@"endTime"] ? infoDict[@"endTime"] : @"";
    self.mediaType = infoDict[@"mediaType"] ? infoDict[@"mediaType"] : @"";
    self.values = infoDict[@"values"] ? infoDict[@"values"] : @"[]";
    return self;
    
}
@end
