//
//  StringeeSDP.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeSDP.h"

@implementation StringeeSDP

- (instancetype)initWithType:(StringeeSdpType)type sdp:(NSString *)sdp {
    if ((self = [super init])) {
        self.sdp = sdp;
        self.type = type;
    }
    
    return self;
}



@end
