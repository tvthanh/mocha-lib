//
//  StringeeVideoView.h
//  TestWebrtcnew
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRTC.h"
#import "StringeeVideoRenderer.h"

@class StringeeVideoView;
@protocol StringeeVideoViewDelegate <RTCEAGLVideoViewDelegate>

- (void)videoView:(StringeeVideoView *)videoView didChangeVideoSize:(CGSize)size;

@end




@interface StringeeVideoView : RTCEAGLVideoView <StringeeVideoRenderer>

@property(nonatomic, weak) id<StringeeVideoViewDelegate> delegate;

@end
