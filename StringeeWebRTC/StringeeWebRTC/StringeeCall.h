//
//  StringeeCall.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PeerConnectionDelegate.h"
#import "WebRTC.h"

#import "StringeeCallFactory.h"

#import "SessionDescriptionDelegate.h"
#import "StringeeSDP.h"

#import "StringeeIceServer.h"
#import "StringeeCallData.h"

#import "StringeeVideoTrack.h"

#import "SignalingStatisticElement.h"
#import "Chart.h"

typedef NS_ENUM(NSInteger, StringeeConnectionState) {
    StringeeConnectionStateNew,
    StringeeConnectionStateChecking,
    StringeeConnectionStateConnected,
    StringeeConnectionStateCompleted,
    StringeeConnectionStateFailed,
    StringeeConnectionStateDisconnected,
    StringeeConnectionStateClosed,
    StringeeConnectionStateCount,
    StringeeConnectionStateRestartICE,
};

//Delegate +++++++++++ ++>
@protocol StringeeCallDelegate <NSObject>

@required
- (void)didGenerateIceCandidate:(StringeeIceCandidate*)iceCandidate;
- (void)didRemoveIceCandidate:(NSArray<StringeeIceCandidate *> *)candidates;
- (void)didCreateSDP:(StringeeSDP*)sdp;
- (void)didCreateLogRTCCall:(NSString *)sLog;
- (void)setCreateSetSdp:(BOOL)isSetSdp;
- (void)logVerboseCallStringee:(NSString *)sLog;
- (void)logLocalTrackEnable:(BOOL)isEnable;

@optional
- (void)didChangeConnectionState:(StringeeConnectionState)newState;

//video call
- (void)didReceiveRemoteVideoTrack:(StringeeVideoTrack*)videoTrack;
- (void)didReceiveLocalVideoWithCaptureSession:(AVCaptureSession *)captureSession;

@end
//Delegate +++++++++++ <++



@interface StringeeCall : NSObject {
    
    PeerConnectionDelegate* peerConnectionDelegate;
    
    BOOL isCaller;
    
    
    
    RTCAVFoundationVideoSource *videoSource;
    
}

@property (assign, nonatomic) BOOL isVideoCall;

@property (assign, nonatomic) BOOL isCallout;
@property (assign, nonatomic) BOOL isCallIn;
@property (assign, nonatomic) BOOL iceConnected;

//@property (assign, nonatomic) BOOL continualGathering;


@property (strong, nonatomic) RTCAudioTrack *remoteAudioTrack;
@property (strong, nonatomic) RTCVideoTrack *remoteVideoTrack;
@property (strong, nonatomic) RTCVideoTrack* localVideoTrack;
//config
@property(nonatomic, copy) NSArray<StringeeIceServer *> *iceServers;//defaul = @[]
@property(nonatomic) BOOL useBackCamera;//default = NO

@property(nonatomic, strong) NSMutableArray<RTCIceCandidate*>* queueIceCandidate;

@property (strong, nonatomic) id<StringeeCallDelegate> delegate;


@property (strong, nonatomic) RTCPeerConnection *peerConnection;


@property (strong, nonatomic) SessionDescriptionDelegate* sessionDescriptionDelegate;

@property (strong, nonatomic) NSMutableDictionary* webrtcInternalDumpObj;
@property (strong, nonatomic) NSMutableDictionary* mapChart;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithFactory:(StringeeCallFactory*)stringeeCallFactory;

//- (void)startCall:(BOOL)isCaller videoCall:(BOOL)isVideoCall;
- (void)startCall:(BOOL)isCaller1 videoCall:(BOOL)isVideoCall11 videoWidth: (NSString *) width videoHeight: (NSString *) height isEnableLog:(BOOL)isEnableLog iceTimeOut:(int)iceTimeOut rtcMuxPolicy:(NSString *)sMuxPolicy rtcBundlePolicy:(NSString *)sBundlePolicy rtcIceTransportPolicy:(NSString *)sIceTransportPolicy;

- (void)stopCall;

//restart ICE
- (void)restartICEForFollowCall;

- (void)switchCamera;

- (void)setRemoteDescription:(StringeeSDP *)sdp;
- (void)addIceCandidate:(StringeeIceCandidate*)candidate;

- (BOOL)setLoudspeaker:(BOOL)enable;

- (void)setEnableLogRTCCall:(BOOL)isEnable;
+ (BOOL)audioSessionSetCategory:(NSString *)category error:(NSError **)outError;
+ (BOOL)audioSessionSetActive:(BOOL)active error:(NSError **)outError;
+ (BOOL)audioSessionSetMode:(NSString *)mode error:(NSError **)outError;

- (void)muteUnmute:(BOOL)mute;
- (void)turnOnCamera:(BOOL)state;

- (void)addCallData:(StringeeCallData*)callData;

- (void)statsWithCompletionHandler: (nullable void (^)(NSDictionary<NSString *, NSString *> *values ))completionHandler useVideoTrack:(BOOL) useVideoTrack;
- (void)getStatsStringWithBlock:(nullable void (^)(NSString * _Nullable description))block useVideoTrack:(BOOL)useVideoTrack;
- (void)getBirateStringWithBlock:(void (^)(NSString * _Nullable videoReceive, NSString * _Nonnull audioReceive, NSString * _Nonnull sendVideoReceive, NSString * _Nonnull sendAudioReceive))block;

@end
