//
//  DefaultConstraintsUtil.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "DefaultConstraintsUtil.h"

@implementation DefaultConstraintsUtil

+ (RTCMediaConstraints *)defaultOfferConstraints:(BOOL)isVideoCall {
    NSDictionary *mandatoryConstraints;
    
    if(isVideoCall){
        mandatoryConstraints = @{
                                 @"OfferToReceiveAudio" : @"true",
                                 @"OfferToReceiveVideo" : @"true"
                                 };
    }else{
        mandatoryConstraints = @{
                                 @"OfferToReceiveAudio" : @"true",
                                 @"OfferToReceiveVideo" : @"false"
                                 };
    }
    
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}

+ (RTCMediaConstraints *)defaultOfferConstraintsForRestartICE:(BOOL)isVideoCall
{
    NSDictionary *mandatoryConstraints;
    
    if(isVideoCall) {
        mandatoryConstraints = @{
                                 @"OfferToReceiveAudio" : @"true",
                                 @"OfferToReceiveVideo" : @"true",
                                 @"IceRestart" : @"true",
                                 };
    } else {
        mandatoryConstraints = @{
                                 @"OfferToReceiveAudio" : @"true",
                                 @"OfferToReceiveVideo" : @"false",
                                 @"IceRestart" : @"true",
                                 };
    }
    
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}


+ (RTCMediaConstraints *)defaultOfferConstraintsForRestartICE
{
    NSDictionary *mandatoryConstraints = @{
                                 @"OfferToReceiveAudio" : @"true",
                                 @"OfferToReceiveVideo" : @"false",
                                 @"IceRestart" : @"true",
                                 };
    
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}

+ (RTCMediaConstraints *)defaultMediaStreamConstraints {
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:nil];
    return constraints;
}

+ (RTCMediaConstraints *)resolutionMediaStreamConstraintsWithWidth: (NSString *) width height: (NSString *) height {
    NSDictionary *mandatoryConstraints = @{
                                           kRTCMediaConstraintsMinWidth : width,
                                           kRTCMediaConstraintsMaxWidth : width,
                                           kRTCMediaConstraintsMinHeight : height,
                                           kRTCMediaConstraintsMaxHeight : height
                                           };
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
    return constraints;
}



+ (RTCMediaConstraints *)defaultPeerConnectionConstraints:(BOOL)isCallout
{
    NSDictionary *optionalConstraints = @{@"DtlsSrtpKeyAgreement": @"true"};;
//    if (isCallout) {
//        optionalConstraints = @{@"DtlsSrtpKeyAgreement": @"true"};
//    }else{
//        optionalConstraints = @{@"DtlsSrtpKeyAgreement": @"true"};
//    }
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:optionalConstraints];
    return constraints;
}

+ (RTCMediaConstraints *)defaultAnswerConstraints:(BOOL)isVideoCall {
    return [DefaultConstraintsUtil defaultOfferConstraints:isVideoCall];
}

@end
