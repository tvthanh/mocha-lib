//
//  Chart.h
//  StringeeWebRTC
//
//  Created by Nguyen Huu Tien on 5/31/19.
//  Copyright © 2019 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chart : NSObject

@property (assign, nonatomic) NSString* startTime;
@property (assign, nonatomic) NSString* endTime;
@property (strong, nonatomic) NSString* values;
@property (assign, nonatomic) NSString* mediaType;

-(instancetype)initWithDictionary:(NSDictionary *)infoDict;
@end

NS_ASSUME_NONNULL_END
