//
//  StringeeIceServer.m
//  StringeeWebRtcLib
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeIceServer.h"

@implementation StringeeIceServer

- (instancetype)initWithURLStrings:(NSArray<NSString *> *)urlStrings {
    return [self initWithURLStrings:urlStrings
                           username:nil
                         credential:nil];
}

- (instancetype)initWithURLStrings:(NSArray<NSString *> *)urlStrings username:(NSString *)username credential:(NSString *)credential {
    if (self = [super init]) {
        _urlStrings = [[NSArray alloc] initWithArray:urlStrings copyItems:YES];
        _username = [username copy];
        _credential = [credential copy];
    }
    return self;
}

@end
