//
//  MochaCallUtils.h
//  StringeeWebRTC
//
//  Created by Duc Nguyen on 1/25/18.
//  Copyright © 2018 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (MochaCallUtils)

// Creates a dictionary with the keys and values in the JSON object.
+ (NSDictionary *)dictionaryWithJSONString:(NSString *)jsonString;
+ (NSDictionary *)dictionaryWithJSONData:(NSData *)jsonData;

@end

@interface NSURLConnection (MochaCallUtils)

// Issues an asynchronous request that calls back on main queue.
+ (void)sendAsyncRequest:(NSURLRequest *)request
       completionHandler:(void (^)(NSURLResponse *response,
                                   NSData *data,
                                   NSError *error))completionHandler;

// Posts data to the specified URL.
+ (void)sendAsyncPostToURL:(NSURL *)url
                  withData:(NSData *)data
         completionHandler:(void (^)(BOOL succeeded,
                                     NSData *data))completionHandler;

@end

NSInteger MochaGetCpuUsagePercentage();
