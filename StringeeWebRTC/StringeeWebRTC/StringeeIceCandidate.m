//
//  StringeeIceCandidate.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeIceCandidate.h"

@implementation StringeeIceCandidate

- (instancetype)initWithSdp:(NSString *)sdp sdpMLineIndex:(int)sdpMLineIndex sdpMid:(NSString *)sdpMid {
    if ((self = [super init])) {
        self.sdp = sdp;
        self.sdpMLineIndex = sdpMLineIndex;
        self.sdpMid = sdpMid;
    }
    
    return self;
}

@end
