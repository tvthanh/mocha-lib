//
//  MochaBitrateTracker.h
//  StringeeWebRTC
//
//  Created by Duc Nguyen on 1/25/18.
//  Copyright © 2018 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MochaBitrateTracker : NSObject

/** The bitrate in bits per second. */
@property(nonatomic, readonly) double bitrate;
/** The bitrate as a formatted string in bps, Kbps or Mbps. */
@property(nonatomic, readonly) NSString *bitrateString;

/** Converts the bitrate to a readable format in bps, Kbps or Mbps. */
+ (NSString *)bitrateStringForBitrate:(double)bitrate;
/** Updates the tracked bitrate with the new byte count. */
- (void)updateBitrateWithCurrentByteCount:(NSInteger)byteCount;
- (void)updateBitrateWithReceiveCurrentByteCount:(NSInteger)byteCount;

@end
