//
//  PeerConnectionDelegate.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 6/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "PeerConnectionDelegate.h"
#import "StringeeIceCandidate.h"
#import "StringeeCall.h"


@implementation PeerConnectionDelegate

- (instancetype)initWithCall:(StringeeCall*)call1 {
    if ((self = [super init])) {
        self.call = call1;
    }
    
    return self;
}

/** Called when the SignalingState changed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeSignalingState:(RTCSignalingState)stateChanged {
    NSLog(@"didChangeSignalingState: %ld", (long)stateChanged);
}

/** Called when media is received on a new stream from remote peer. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didAddStream:(RTCMediaStream *)stream {
    
    if (stream.audioTracks.count > 0) {
        self.call.remoteAudioTrack = stream.audioTracks[0];
        NSLog(@"set remoteAudioTrack +++++++++++++++++++++++++");
    }
    
    if (stream.videoTracks.count > 0) {
        self.call.remoteVideoTrack = stream.videoTracks[0];
        NSLog(@"set remoteVideoTrack +++++++++++++++++++++++++");
    }
    
//    NSLog(@"Received %lu video tracks and %lu audio tracks", (unsigned long)stream.videoTracks.count, (unsigned long)stream.audioTracks.count);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (stream.videoTracks.count) {
            for (RTCVideoTrack* track1 in stream.videoTracks) {
                StringeeVideoTrack* track = [[StringeeVideoTrack alloc] initWithRTCVideoTrack:track1];
                if (self.call.delegate) {
                    [self.call.delegate didReceiveRemoteVideoTrack:track];
                }
            }
        }
        
    });
}

/** Called when a remote peer closes a stream. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveStream:(RTCMediaStream *)stream {
    NSLog(@"didRemoveStream");
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection {
    NSLog(@"peerConnectionShouldNegotiate");
}

/** Called any time the IceConnectionState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceConnectionState:(RTCIceConnectionState)newState {
//    NSLog(@"++++++++++++++++++++++++++++++++++++++++ didChangeIceConnectionState: %ld", (long)newState);
    
    if (newState == RTCIceConnectionStateConnected) {
        self.call.iceConnected = YES;
    }
    
    if (self.call.delegate) {
        [self.call.delegate didChangeConnectionState:(int)newState];
    }
    
    if (newState == RTCIceConnectionStateConnected) {
        //connected add bu` candidate
        while (self.call.queueIceCandidate.count > 0) {
            RTCIceCandidate* candidate = [self.call.queueIceCandidate objectAtIndex:0];
//            NSLog(@"+++++++++++++++++++++++++++++add bu` candidate: %@", candidate.sdp);
            [self.call.queueIceCandidate removeObjectAtIndex:0];
            [self.call.peerConnection addIceCandidate:candidate];
        }
    }
}

/** Called any time the IceGatheringState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didChangeIceGatheringState:(RTCIceGatheringState)newState {
    NSLog(@"didChangeIceGatheringState: %ld", (long)newState);
}

/** New ice candidate has been found. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didGenerateIceCandidate:(RTCIceCandidate *)candidate {
    NSLog(@"didGenerateIceCandidate +++++++++++++++++iceGatheringState: %ld", peerConnection.iceGatheringState);
    NSLog(@"didGenerateIceCandidate +++++++++++++++++iceConnectionState: %ld", peerConnection.iceConnectionState);
    
    StringeeIceCandidate* stringeeCandidate = [[StringeeIceCandidate alloc] initWithSdp:candidate.sdp sdpMLineIndex:candidate.sdpMLineIndex sdpMid:candidate.sdpMid];
    if (self.call.delegate) {
        [self.call.delegate didGenerateIceCandidate:stringeeCandidate];
    }
}

/** Called when a group of local Ice candidates have been removed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didRemoveIceCandidates:(NSArray<RTCIceCandidate *> *)candidates
{
    NSLog(@"didRemoveIceCandidates");
    @try {
        if (self.call.delegate) {
            [self.call.delegate logVerboseCallStringee:@"didRemoveIceCandidates"];
            NSMutableArray *arrCandidates = [NSMutableArray array];
            for (RTCIceCandidate *candidate in candidates) {
                StringeeIceCandidate* stringeeCandidate = [[StringeeIceCandidate alloc] initWithSdp:candidate.sdp sdpMLineIndex:candidate.sdpMLineIndex sdpMid:candidate.sdpMid];
                [arrCandidates addObject:stringeeCandidate];
            }
            [self.call.delegate didRemoveIceCandidate:[arrCandidates copy]];
        }
    } @catch (NSException *exception) {
        [self.call.delegate logVerboseCallStringee:exception.description];
    } @finally {
        
    }
}

/** New data channel has been opened. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection didOpenDataChannel:(RTCDataChannel *)dataChannel {
}

@end
