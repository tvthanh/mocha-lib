//
//  StringeeVideoTrack.h
//  TestWebrtcnew
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RTCVideoTrack.h"

#import "StringeeVideoRenderer.h"

@interface StringeeVideoTrack : NSObject {
    RTCVideoTrack* rtcVideoTrack;
}

- (instancetype)initWithRTCVideoTrack:(RTCVideoTrack *)rtcVideoTrack;

- (void)addRenderer:(id<StringeeVideoRenderer>)renderer;

- (void)removeRenderer:(id<StringeeVideoRenderer>)renderer;

@end
