//
//  StringeeCallData.h
//  FClient
//
//  Created by Andy Dau on 9/28/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringeeCallData : NSObject

@property (strong, nonatomic) NSString* type;//candidate | sdp
@property (strong, nonatomic) NSString* data;

- (instancetype)initWithType:(NSString *)type data:(NSString *)data;

@end
