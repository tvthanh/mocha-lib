//
//  StringeeSDP.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, StringeeSdpType) {
    StringeeSdpTypeOffer,
    StringeeSdpTypePrAnswer,
    StringeeSdpTypeAnswer,
};

@interface StringeeSDP : NSObject


@property(nonatomic) StringeeSdpType type;

@property(nonatomic) NSString *sdp;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithType:(StringeeSdpType)type sdp:(NSString *)sdp;


@end
