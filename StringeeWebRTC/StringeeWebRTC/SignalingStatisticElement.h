//
//  SignalingStatisticElement.h
//  StringeeWebRTC
//
//  Created by Nguyen Huu Tien on 5/29/19.
//  Copyright © 2019 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignalingStatisticElement : NSObject

@property (assign, nonatomic) NSString* time;
@property (assign, nonatomic) NSString* type;
@property (assign, nonatomic) NSString* value;

@end

NS_ASSUME_NONNULL_END
