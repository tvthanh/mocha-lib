//
//  StringeeCameraPreviewView.h
//  TestWebrtcnew
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRTC.h"

@interface StringeeCameraPreviewView : RTCCameraPreviewView

//@property(nonatomic, strong) AVCaptureSession *captureSession;

@end
