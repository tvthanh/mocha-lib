//
//  PeerConnectionDelegate.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 6/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RTCPeerConnection.h"

@class StringeeCall;

@interface PeerConnectionDelegate : NSObject <RTCPeerConnectionDelegate>

@property (weak, nonatomic) StringeeCall* call;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithCall:(StringeeCall*)call;

@end
