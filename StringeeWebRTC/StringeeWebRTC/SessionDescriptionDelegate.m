//
//  SessionDescriptionDelegate.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/16/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "SessionDescriptionDelegate.h"

#import "SdpUtil.h"
#import "StringeeCallFactory.h"
#import "DefaultConstraintsUtil.h"

#import "StringeeCall.h"
#import "StringeeSDP.h"

@implementation SessionDescriptionDelegate

- (SessionDescriptionDelegate*)initAsCaller:(BOOL)isCaller1 call:(StringeeCall*)call1 {
    if ((self = [super init])) {
        isCaller = isCaller1;
        self.call = call1;
    }
    
    return self;
}

/**
    Ham nay goi khi tao thanh cong SDP (de offer hoac answer): luc nay can set SDP thanh localSDP cua PC
 **/
- (void)didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error isVideoCall:(BOOL)isVideoCall {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"error: %@", error);
            return;
        }
        
        if (sdp.type == RTCSdpTypeOffer) {
            //            NSLog(@"Create Offer SDP OK ==================================================");
        }else{
            //            NSLog(@"Create Answer SDP OK ==================================================");
        }
        RTCSessionDescription *sdpPrefer;
//        if (isCaller) {
            if ([StringeeCallFactory instance].preferredVideoCodec &&[StringeeCallFactory instance].preferredVideoCodec.length) {
                // Cần ưu tiên video codec
                sdpPrefer = [SdpUtil descriptionForDescription:sdp preferredVideoCodec:[StringeeCallFactory instance].preferredVideoCodec];
            } else {
                sdpPrefer = [SdpUtil descriptionForDescription:sdp preferredVideoCodec:@"VP8"];;
            }

            if ([StringeeCallFactory instance].preferredAudioCodec && [StringeeCallFactory instance].preferredAudioCodec.length) {
                // Cần ưu tiên audio codec
                sdpPrefer = [SdpUtil descriptionForDescription:sdpPrefer preferredAudioCodec:[StringeeCallFactory instance].preferredAudioCodec];
            } else {
                sdpPrefer = [SdpUtil descriptionForDescription:sdpPrefer preferredAudioCodec:@"opus"];
            }
//
//        } else {
//            sdpPrefer = sdp;
//        }
        
        __weak SessionDescriptionDelegate *weakSelf = self;
        [self.call.peerConnection setLocalDescription:sdpPrefer completionHandler:^(NSError *error) {
            //set thanh cong
            SessionDescriptionDelegate *strongSelf = weakSelf;
            [strongSelf didSetLocalSDPCallBack:error local:YES offer:(sdp.type == RTCSdpTypeOffer) isVideoCall:isVideoCall];
            
            if (!error) {
                if (self.call.delegate) {
                    StringeeSdpType type1;
                    if (sdp.type == RTCSdpTypeOffer) {
                        type1 = StringeeSdpTypeOffer;
                    }else if (sdp.type == RTCSdpTypeAnswer){
                        type1 = StringeeSdpTypeAnswer;
                    }else{
                        type1 = StringeeSdpTypePrAnswer;
                    }
                    StringeeSDP* stringeeSdp = [[StringeeSDP alloc] initWithType:type1 sdp:sdpPrefer.sdp];
                    [self.call.delegate didCreateSDP:stringeeSdp];
                }
            } else {
                // phải sử lý end call ở đây nhé.
            }
            
        }];
    });
}

- (void)didSetLocalSDPCallBack:(NSError *)error local:(BOOL)setLocalSdp offer:(BOOL)setOfferSdp isVideoCall:(BOOL)isVideoCall{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"didSetLocalSDPCallBack: %@", error);
            return;
        }
    });
}

- (void)didSetRemoteSDPCallBack:(NSError *)error local:(BOOL)setLocalSdp offer:(BOOL)setOfferSdp isVideoCall:(BOOL)isVideoCall{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"didSetRemoteSDPCallBack: %@", error);
            return;
        }
        if (!isCaller) {
            RTCMediaConstraints *constraints = [DefaultConstraintsUtil defaultAnswerConstraints:isVideoCall];
            __weak SessionDescriptionDelegate *weakSelf = self;
            [self.call.peerConnection answerForConstraints:constraints completionHandler:^(RTCSessionDescription *sdp, NSError *error) {
                SessionDescriptionDelegate *strongSelf = weakSelf;
                [strongSelf didCreateSessionDescription:sdp error:error isVideoCall:isVideoCall];
            }];
        }
    });
}




@end
