//
//  StringeeCallData.m
//  FClient
//
//  Created by Andy Dau on 9/28/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeCallData.h"

@implementation StringeeCallData

- (instancetype)initWithType:(NSString *)type data:(NSString *)data {
    if ((self = [super init])) {
        self.type = type;
        self.data = data;
    }
    
    return self;
}

@end
