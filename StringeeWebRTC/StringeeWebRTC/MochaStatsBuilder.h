//
//  MochaStatsBuilder.h
//  StringeeWebRTC
//
//  Created by Duc Nguyen on 1/25/18.
//  Copyright © 2018 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RTCLegacyStatsReport;

@interface MochaStatsBuilder : NSObject

/** String that represents the accumulated stats reports passed into this
 *  class.
 */
@property(nonatomic, readonly) NSString *statsString;
@property (nonatomic, strong) NSMutableArray *videoRecvArray, *audioRecvArray;

/** Parses the information in the stats report into an appropriate internal
 *  format used to generate the stats string.
 */
- (void)parseStatsReport:(RTCLegacyStatsReport *)statsReport;
- (NSString *)getReceiveVideoBirate;
- (NSString *)getReceiveAudioBirate;
- (NSString *)getSendVideoBirate;
- (NSString *)getSendAudioBirate;

@end
