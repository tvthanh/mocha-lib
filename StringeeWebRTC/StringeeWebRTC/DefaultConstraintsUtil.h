//
//  DefaultConstraintsUtil.h
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebRTC.h"

@interface DefaultConstraintsUtil : NSObject

+ (RTCMediaConstraints *)defaultOfferConstraints:(BOOL)isVideoCall;
+ (RTCMediaConstraints *)defaultAnswerConstraints:(BOOL)isVideoCall;
+ (RTCMediaConstraints *)defaultOfferConstraintsForRestartICE:(BOOL)isVideoCall;
+ (RTCMediaConstraints *)defaultOfferConstraintsForRestartICE;
+ (RTCMediaConstraints *)defaultMediaStreamConstraints;
+ (RTCMediaConstraints *)defaultPeerConnectionConstraints:(BOOL)isCallout;

+ (RTCMediaConstraints *)resolutionMediaStreamConstraintsWithWidth: (NSString *) width height: (NSString *) height;
@end
