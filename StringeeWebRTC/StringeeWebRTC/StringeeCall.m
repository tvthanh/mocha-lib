//
//  StringeeCall.m
//  TestWebrtcnew
//
//  Created by Dau Ngoc Huy on 9/17/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeCall.h"
#import "DefaultConstraintsUtil.h"
#import "SdpUtil.h"
#import "MochaStatsBuilder.h"
#import "RTCFileLogger.h"
#import "MochaCallUtils.h"

@implementation StringeeCall {
    StringeeCallFactory* callFactory;
    BOOL callStarted;
    RTCAudioTrack *localAudioTrack;
    MochaStatsBuilder *statsBuilder;
    RTCFileLogger *logFileRTC;
    BOOL isEnableLogRTCCall;
}

static NSString * const kARDMediaStreamId = @"ARDAMS";
static NSString * const kARDAudioTrackId = @"ARDAMSa0";
static NSString * const kARDVideoTrackId = @"ARDAMSv0";

- (instancetype)initWithFactory:(StringeeCallFactory*)stringeeCallFactory {
    if ((self = [super init])) {
        callFactory = stringeeCallFactory;
        self.queueIceCandidate = [[NSMutableArray alloc] init];
        callStarted = NO;
        self.isCallout = NO;
        //        self.continualGathering = YES;//free call thi lien tuc lay candidate
        statsBuilder = [[MochaStatsBuilder alloc] init];
        self.isVideoCall = NO;
        self.isCallIn = NO;
        self.webrtcInternalDumpObj = [self createDumpObject];
        self.mapChart = [[NSMutableDictionary alloc] init];
        
        self.useBackCamera = NO;
//        logFileRTC = [[RTCFileLogger alloc] initWithDirPath:[self filePathForRTCCallLog:@"RTCLogFile"] maxFileSize:10*1024*1024];
    }

    return self;
}

- (void)startCall:(BOOL)isCaller1 videoCall:(BOOL)isVideoCall11 videoWidth: (NSString *) width videoHeight: (NSString *) height isEnableLog:(BOOL)isEnableLog iceTimeOut:(int)iceTimeOut rtcMuxPolicy:(NSString *)sMuxPolicy rtcBundlePolicy:(NSString *)sBundlePolicy rtcIceTransportPolicy:(NSString *)sIceTransportPolicy
{
    isEnableLogRTCCall = isEnableLog;
    if (isEnableLogRTCCall) {
        RTCSetMinDebugLogLevel(RTCLoggingSeverityVerbose);
    }
    
    // videocall: YES -> call data
    // videocall: NO -> call out
    
    isCaller = isCaller1;
    self.iceConnected = NO;
    
    self.isVideoCall = isVideoCall11;
    
    // Create peer connection.
//    RTCConfiguration *config = [[RTCConfiguration alloc] init];
//    config.rtcpMuxPolicy = RTCRtcpMuxPolicyRequire;
//#warning - Hieuhd5 sua config test
//    // config.iceRegatherIntervalRange = [[RTCIntervalRange alloc] initWithMin:5000 max:30000];
//    if (self.isCallout) {
//        config.bundlePolicy = RTCBundlePolicyMaxBundle;
//        config.iceTransportPolicy = RTCIceTransportPolicyNoHost;
//    }
//    else {
//        config.bundlePolicy = RTCBundlePolicyBalanced;
//        if (iceTimeOut > 0) {
//            config.iceConnectionReceivingTimeout = iceTimeOut;
//        }
//    }

    
    // Create peer connection.
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
//    config.rtcpMuxPolicy = RTCRtcpMuxPolicyRequire;
//    config.iceTransportPolicy = RTCIceTransportPolicyNoHost;
    config.rtcpMuxPolicy = [self getRtcpMuxPolicyWithData:sMuxPolicy];
    config.iceTransportPolicy = [self getIceTransportsTypeWithData:sIceTransportPolicy];
#warning - Hieuhd5 sua config test
    // config.iceRegatherIntervalRange = [[RTCIntervalRange alloc] initWithMin:5000 max:30000];
    if (self.isCallout) {
        config.bundlePolicy = [self getRtcBundlePolicyWithData:sBundlePolicy];
    }
    else {
        config.bundlePolicy = [self getRtcBundlePolicyWithData:sBundlePolicy];
        if (iceTimeOut > 0) {
            config.iceConnectionReceivingTimeout = iceTimeOut;
        }
    }

    // het Hieuhd5 sua config
    
    if (self.iceServers) {
        NSMutableArray<RTCIceServer*>* rtcIceServers = [[NSMutableArray alloc] init];
        for (StringeeIceServer* iceServer in  self.iceServers) {
            RTCIceServer* rtcIceServer = [[RTCIceServer alloc] initWithURLStrings:iceServer.urlStrings username:iceServer.username credential:iceServer.credential];
            [rtcIceServers addObject:rtcIceServer];
        }
        config.iceServers = rtcIceServers;
    }
    
    //disable tcp
    config.tcpCandidatePolicy = RTCTcpCandidatePolicyDisabled;
    
//    if (!self.isCallout) {
        //        config.iceTransportPolicy = RTCIceTransportPolicyNoHost;//de dong nay ko call sang dc firefox
//    }
    
    //    config.candidateNetworkPolicy = RTCCandidateNetworkPolicyLowCost;//de dong nay khong sinh candidate trong 1 so truong hop
    
    config.continualGatheringPolicy = RTCContinualGatheringPolicyGatherContinually;//tat mang bat lai thi tu lay lai candidate
    
    
    //    config.iceConnectionReceivingTimeout = 5;
    
    RTCMediaConstraints *constraints = [DefaultConstraintsUtil defaultPeerConnectionConstraints:self.isCallout];
    //delegate
    peerConnectionDelegate = [[PeerConnectionDelegate alloc] initWithCall:self];
    self.peerConnection = [callFactory.factory peerConnectionWithConfiguration:config constraints:constraints delegate:peerConnectionDelegate];
    
    //Start Log
    if (isEnableLogRTCCall) {
        if (!logFileRTC) {
            logFileRTC = [[RTCFileLogger alloc] initWithDirPath:[self filePathForRTCCallLog:[NSString stringWithFormat:@"RTCLogFile"]] maxFileSize:10*1024*1024];
        }
        logFileRTC.severity = RTCLoggingSeverityVerbose;
        [logFileRTC start];
    }

    //+++++++++++++++++++++++++++++++++++++++++\\
    
    // Create AV senders.
    [self createAudioSender];
    if (isVideoCall11) {
        [self createVideoSenderWith:width height:height];
    }
    
    self.sessionDescriptionDelegate = [[SessionDescriptionDelegate alloc] initAsCaller:isCaller call:self];
    
    //Send offer.
    if (isCaller) {
        __weak StringeeCall *weakSelf = self;
        [self.peerConnection offerForConstraints:[DefaultConstraintsUtil defaultOfferConstraints:isVideoCall11] completionHandler:^(RTCSessionDescription *sdp, NSError *error) {
            
            StringeeCall *strongSelf = weakSelf;
            
            [strongSelf.sessionDescriptionDelegate didCreateSessionDescription:sdp error:error isVideoCall:isVideoCall11];
            
        }];
    }
    
    callStarted = YES;
}

- (RTCBundlePolicy)getRtcBundlePolicyWithData:(NSString *)sBundlePolicy
{
    //    BALANCED|MAXBUNDLE|MAXCOMPAT
    if ([sBundlePolicy isEqualToString:@"BALANCED"]) {
        return RTCBundlePolicyBalanced;
    } else if ([sBundlePolicy isEqualToString:@"MAXBUNDLE"]) {
        return RTCBundlePolicyMaxBundle;
    } else if ([sBundlePolicy isEqualToString:@"MAXCOMPAT"]) {
        return RTCBundlePolicyMaxCompat;
    } else {
        return RTCBundlePolicyBalanced;
    }
}

- (RTCRtcpMuxPolicy)getRtcpMuxPolicyWithData:(NSString *)sRtcMuxPolicy
{
    //    NEGOTIATE|REQUIRE
    if ([sRtcMuxPolicy isEqualToString:@"NEGOTIATE"]) {
        return RTCRtcpMuxPolicyNegotiate;
    } else if ([sRtcMuxPolicy isEqualToString:@"REQUIRE"]) {
        return RTCRtcpMuxPolicyRequire;
    }  else {
        return RTCRtcpMuxPolicyRequire;
    }
}

- (RTCIceTransportPolicy)getIceTransportsTypeWithData:(NSString *)sBundlePolicy
{
    //    NONE|RELAY|NOHOST|ALL
    if ([sBundlePolicy isEqualToString:@"NONE"]) {
        return RTCIceTransportPolicyNone;
    } else if ([sBundlePolicy isEqualToString:@"RELAY"]) {
        return RTCIceTransportPolicyRelay;
    } else if ([sBundlePolicy isEqualToString:@"NOHOST"]) {
        return RTCIceTransportPolicyNoHost;
    } else if ([sBundlePolicy isEqualToString:@"ALL"]) {
        return RTCIceTransportPolicyAll;
    }
    else {
        if (self.isCallout || self.isCallIn) {
            return RTCIceTransportPolicyNoHost;
        } else {
            return RTCIceTransportPolicyRelay;
        }
    }
}

- (void)restartICEForFollowCall
{
//    if (isCaller) {
        __weak StringeeCall *weakSelf = self;
        [self.peerConnection offerForConstraints:[DefaultConstraintsUtil defaultOfferConstraintsForRestartICE:self.isVideoCall] completionHandler:^(RTCSessionDescription *sdp, NSError *error) {
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(setCreateSetSdp:)]) {
                [self.delegate setCreateSetSdp:YES];
            }
            StringeeCall *strongSelf = weakSelf;
            
            [strongSelf.sessionDescriptionDelegate didCreateSessionDescription:sdp error:error isVideoCall:NO];
            
        }];
//    }
}

- (void)stopCall {
    if (!callStarted) {
        NSLog(@"Call hasn't started yet");
        return;
    }
    callStarted = NO;
    self.iceConnected = NO;
    
    [self.peerConnection close];
    
    self.peerConnection = nil;
    peerConnectionDelegate = nil;
    localAudioTrack = nil;
    _remoteAudioTrack = nil;
    _remoteVideoTrack = nil;
    
    if (isEnableLogRTCCall) {
        [logFileRTC stop];
        NSString* sLogData = [[NSString alloc] initWithData:[logFileRTC logData] encoding:NSUTF8StringEncoding];
        if (self.delegate && [self.delegate respondsToSelector:@selector(didCreateLogRTCCall:)]) {
            [self.delegate didCreateLogRTCCall:sLogData];
        }
    }
//    NSLog(@"dumpReport: %@", [self getJsonStringFromNSDictionaty:self.webrtcInternalDumpObj]);
}

- (void)setEnableLogRTCCall:(BOOL)isEnable
{
    isEnableLogRTCCall = isEnable;
}

- (RTCRtpSender *)createAudioSender {
    RTCRtpSender *sender = [self.peerConnection senderWithKind:kRTCMediaStreamTrackKindAudio streamId:kARDMediaStreamId];
    
    localAudioTrack = [callFactory.factory audioTrackWithTrackId:kARDAudioTrackId];
    sender.track = localAudioTrack;
    return sender;
}

- (void)setRemoteDescription:(StringeeSDP *)sdp {
    
    //    NSLog(@"--setRemoteDescription: type:%ld, sdp:%@", (long)sdp.type, sdp.sdp);
    
    RTCSessionDescription* desc = [[RTCSessionDescription alloc] initWithType:(int)sdp.type sdp:sdp.sdp];
    
    //    NSLog(@"desc===%@", desc);
    
    __weak StringeeCall *weakSelf = self;
    
    [self.peerConnection setRemoteDescription:desc completionHandler:^(NSError *error){
        
        StringeeCall *strongSelf = weakSelf;
        [strongSelf.sessionDescriptionDelegate didSetRemoteSDPCallBack:error local:NO offer:(desc.type == RTCSdpTypeOffer) isVideoCall:strongSelf.isVideoCall];
        
    }];
}

- (void)addIceCandidate:(StringeeIceCandidate*)candidate1 {
    RTCIceCandidate* candidate = [[RTCIceCandidate alloc] initWithSdp:candidate1.sdp sdpMLineIndex:candidate1.sdpMLineIndex sdpMid:candidate1.sdpMid];
    
    //neu la callout, add luon
    if (self.isCallout) {
        __weak StringeeCall *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            StringeeCall *strongSelf = weakSelf;
            [strongSelf.peerConnection addIceCandidate:candidate];
        });
        return;
    }

    // Comment chỗ này thì không ưu tiên relay
    NSArray<NSString*>* tmp = [candidate1.sdp componentsSeparatedByString:@" "];
    NSString* transport = tmp[2];
    NSString* candType = tmp[7];
    //    NSString* ip = tmp[4];
    if ([transport isEqualToString:@"udp"] && [candType isEqualToString:@"relay"]) {
        //        NSLog(@"nhan dc candidate: transport=%@, candType=%@, ip=%@, add +++++++++++++++++++++++++++++++++++++++++", transport, candType, ip);
        __weak StringeeCall *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            StringeeCall *strongSelf = weakSelf;
            [strongSelf.peerConnection addIceCandidate:candidate];
        });
    }
    // test chi add relay
    /*
    else if (self.iceConnected){
        //        NSLog(@"nhan dc candidate: transport=%@, candType=%@, ip=%@, add luon vi ice da connected +++++++++++++++++++++++++", transport, candType, ip);
    } else {
        //        NSLog(@"nhan dc candidate: transport=%@, candType=%@, ip=%@, don't add -----------------------------------", transport, candType, ip);
        //add vao queue
        [self.queueIceCandidate addObject:candidate];
        
        return;
    }
    
    
    
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\\
    
    __weak StringeeCall *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        StringeeCall *strongSelf = weakSelf;
        [strongSelf.peerConnection addIceCandidate:candidate];
    });
     */
}

- (void)removeIceCandidate:(NSArray<StringeeIceCandidate *> *)arrCandidate
{
    NSMutableArray *arrRtcIceCandidate = [NSMutableArray array];
    for (StringeeIceCandidate *iceCandidate in arrCandidate) {
        RTCIceCandidate* candidate = [[RTCIceCandidate alloc] initWithSdp:iceCandidate.sdp sdpMLineIndex:iceCandidate.sdpMLineIndex sdpMid:iceCandidate.sdpMid];
        [arrRtcIceCandidate addObject:candidate];
    }
    
    //neu la callout, add luon
    if (self.isCallout) {
        return;
    }
    
    __weak StringeeCall *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        StringeeCall *strongSelf = weakSelf;
        [strongSelf.peerConnection removeIceCandidates:[arrRtcIceCandidate copy]];
    });
}

- (BOOL)setLoudspeaker:(BOOL)enable {
    RTCAudioSession* session = [RTCAudioSession sharedInstance];
    [session lockForConfiguration];
    
    AVAudioSessionCategoryOptions options = session.categoryOptions;
    
    /* Doan code nay bi sai neu category cu ko phai la PlayAndRecord
     NSString* category = session.category;
     // Respect old category options if category is
     // AVAudioSessionCategoryPlayAndRecord. Otherwise reset it since old options
     // might not be valid for this category.
     if ([category isEqualToString:AVAudioSessionCategoryPlayAndRecord]) {
     if (enable) {
     options |= AVAudioSessionCategoryOptionDefaultToSpeaker;
     } else {
     options &= ~AVAudioSessionCategoryOptionDefaultToSpeaker;
     }
     } else {
     options = AVAudioSessionCategoryOptionDefaultToSpeaker;
     }
     */
    
    if (enable) {
        options |= AVAudioSessionCategoryOptionDefaultToSpeaker;
    } else {
        options &= ~AVAudioSessionCategoryOptionDefaultToSpeaker;
    }
    
    NSError* error = nil;
    BOOL success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                            withOptions:options
                                  error:&error];
    
    [session unlockForConfiguration];
    return success;
}

+ (BOOL)audioSessionSetCategory:(NSString *)category error:(NSError **)outError {
    RTCAudioSession* session = [RTCAudioSession sharedInstance];
    [session lockForConfiguration];
    
    //    NSError* error = nil;
    AVAudioSessionCategoryOptions options = session.categoryOptions;
    
    BOOL success = [session setCategory:category
                            withOptions:options
                                  error:outError];
    
    [session unlockForConfiguration];
    return success;
}

+ (BOOL)audioSessionSetActive:(BOOL)active error:(NSError **)outError {
    RTCAudioSession* session = [RTCAudioSession sharedInstance];
    [session lockForConfiguration];
    
    BOOL success = [session setActive:active error:outError];
    
    [session unlockForConfiguration];
    return success;
}

+ (BOOL)audioSessionSetMode:(NSString *)mode error:(NSError **)outError {
    RTCAudioSession* session = [RTCAudioSession sharedInstance];
    [session lockForConfiguration];
    
    BOOL success = [session setMode:mode error:outError];
    
    [session unlockForConfiguration];
    return success;
}

- (void)muteUnmute:(BOOL)mute {
    if(localAudioTrack){
        localAudioTrack.isEnabled = !mute;
        if (self.delegate && [self.delegate respondsToSelector:@selector(logLocalTrackEnable:)]) {
            [self.delegate logLocalTrackEnable:localAudioTrack.isEnabled];
        }
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(logLocalTrackEnable:)]) {
            [self.delegate logLocalTrackEnable:NO];
        }
    }
}

-(void)turnOnCamera:(BOOL) state {
    
    if (self.localVideoTrack) {
        if (state) {
            self.localVideoTrack.isEnabled = YES;
        } else {
            self.localVideoTrack.isEnabled = NO;
        }
    }
}

//++++++++++++++++++++++ helper +++++++++++

- (void)addCallData:(StringeeCallData*)callData {
    NSDictionary* data = [NSJSONSerialization JSONObjectWithData:[callData.data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    if ([callData.type isEqualToString:@"sdp"]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(setCreateSetSdp:)]) {
            [self.delegate setCreateSetSdp:NO];
        }
        [self receiveSdp:data];
    } else if ([callData.type isEqualToString:@"set-sdp"]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(setCreateSetSdp:)]) {
            [self.delegate setCreateSetSdp:YES];
        }
        [self receiveSdp:data];
    } else if ([callData.type isEqualToString:@"candidate"]) {
        [self receiveCandidate:data];
    } else if ([callData.type isEqualToString:@"remove-candidate"]) {
        NSData *dataCandidate = [NSKeyedArchiver archivedDataWithRootObject:data];
        NSArray *arrCandidate = [NSKeyedUnarchiver unarchiveObjectWithData:dataCandidate];
        [self removeCandidate:arrCandidate];
    }
}

- (void)receiveSdp:(NSDictionary*)data {
    NSNumber* sdpType = data[@"type"];
    NSString* sdpString = data[@"sdp"];
    
    //    NSLog(@"+++receiveSdp: %@", data);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        StringeeSDP* stringeeSdp = [[StringeeSDP alloc] initWithType:sdpType.intValue sdp:sdpString];
        [self setRemoteDescription:stringeeSdp];
    });
}

- (void)receiveCandidate:(NSDictionary*)data {
    NSString* sdpMid = data[@"sdpMid"];
    NSNumber* sdpMLineIndex = data[@"sdpMLineIndex"];
    NSString* sdp = data[@"sdp"];
    
    StringeeIceCandidate* iceCandidate = [[StringeeIceCandidate alloc] initWithSdp:sdp sdpMLineIndex:sdpMLineIndex.intValue sdpMid:sdpMid];
    [self addIceCandidate:iceCandidate];
}

- (void)removeCandidate:(NSArray *)arrCandidate
{
    NSMutableArray *arrIceCandidate = [NSMutableArray array];
    for (NSDictionary *dictCandidate in arrCandidate) {
        NSString* sdpMid = dictCandidate[@"sdpMid"];
        NSNumber* sdpMLineIndex = dictCandidate[@"sdpMLineIndex"];
        NSString* sdp = dictCandidate[@"sdp"];
        
        StringeeIceCandidate* iceCandidate = [[StringeeIceCandidate alloc] initWithSdp:sdp sdpMLineIndex:sdpMLineIndex.intValue sdpMid:sdpMid];
        [arrIceCandidate addObject:iceCandidate];
    }
    
    [self removeIceCandidate:[arrIceCandidate copy]];
}

- (void)statsWithCompletionHandler:
(nullable void (^)( NSDictionary<NSString *, NSString *> *values ))completionHandler useVideoTrack:(BOOL) useVideoTrack {
    
    RTCMediaStreamTrack * targetTrack;
    if (useVideoTrack) {
        targetTrack = self.remoteVideoTrack;
    } else {
        targetTrack = self.remoteAudioTrack;
    }
    
    if (targetTrack) {
        [self.peerConnection
         statsForTrack:nil
         statsOutputLevel:RTCStatsOutputLevelDebug
         completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats22){
             
             for (RTCLegacyStatsReport* stats1 in stats22) {
                 
                 if ([stats1.type isEqualToString:@"ssrc"]) {
                     if (completionHandler)
                         completionHandler(stats1.values);
                 }
                 
                 
             }
             
         }];
    }
}

- (void)getStatsStringWithBlock:(void (^)(NSString *description))block useVideoTrack:(BOOL)useVideoTrack
{
    RTCMediaStreamTrack * targetTrack;
    if (useVideoTrack) {
        targetTrack = self.remoteVideoTrack;
    } else {
        targetTrack = self.remoteAudioTrack;
    }
    
    if (targetTrack) {
        [self.peerConnection
         statsForTrack:nil
         statsOutputLevel:RTCStatsOutputLevelDebug
         completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats22){
             for (RTCLegacyStatsReport* stats1 in stats22) {
                 [statsBuilder parseStatsReport:stats1];
                 if (block) block(statsBuilder.statsString);
             }
         }];
    }
}

- (void)getBirateStringWithBlock:(void (^)(NSString * _Nullable, NSString * _Nonnull, NSString * _Nonnull, NSString * _Nonnull))block
{
    [self.peerConnection
     statsForTrack:nil
     statsOutputLevel:RTCStatsOutputLevelDebug
     completionHandler:^(NSArray<RTCLegacyStatsReport *> *stats22){
         // reset recv array
         [statsBuilder.videoRecvArray removeAllObjects];
         [statsBuilder.audioRecvArray removeAllObjects];
         
         // parse
         for (RTCLegacyStatsReport* stats1 in stats22) {
             [self createDumpReport:stats1];
             [statsBuilder parseStatsReport:stats1];
         }
         
         NSLog(@"VIDEO RECV: %@", statsBuilder.videoRecvArray);
         NSLog(@"AUDIO RECV: %@", statsBuilder.audioRecvArray);
         //
         if (block) block([statsBuilder getReceiveVideoBirate], [statsBuilder getReceiveAudioBirate], [statsBuilder getSendVideoBirate], [statsBuilder getSendAudioBirate]);
     }];
}

- (RTCRtpSender *)createVideoSenderWith: (NSString *) width height: (NSString *) height {
    RTCRtpSender *sender = [self.peerConnection senderWithKind:kRTCMediaStreamTrackKindVideo streamId:kARDMediaStreamId];
    
    self.localVideoTrack = nil;
#if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
    
    RTCMediaConstraints *mediaConstraints;
    if (width.length && height.length) {
        // Đảo ngược vì thực tế là theo screen của GoogleChrome
        mediaConstraints = [DefaultConstraintsUtil resolutionMediaStreamConstraintsWithWidth:height height:width];
    } else {
        mediaConstraints = [DefaultConstraintsUtil resolutionMediaStreamConstraintsWithWidth:@"640" height:@"480"];
    }
    
    //    mediaConstraints = [DefaultConstraintsUtil defaultMediaStreamConstraints];
    
    videoSource = [callFactory.factory avFoundationVideoSourceWithConstraints:mediaConstraints];
    videoSource.useBackCamera = self.useBackCamera;
    self.localVideoTrack = [callFactory.factory videoTrackWithSource:videoSource trackId:kARDVideoTrackId];
#endif
    
    if (self.localVideoTrack) {
        sender.track = self.localVideoTrack;
        [self didReceiveLocalVideoTrack:self.localVideoTrack];
    }
    return sender;
}

-(void)didReceiveLocalVideoTrack:(RTCVideoTrack *)localVideoTrack {
    if ([localVideoTrack.source isKindOfClass:[RTCAVFoundationVideoSource class]]) {
        RTCAVFoundationVideoSource* source = (RTCAVFoundationVideoSource*)localVideoTrack.source;
        if (self.delegate) {
            [self.delegate didReceiveLocalVideoWithCaptureSession:source.captureSession];
        }
    }
}

- (void)switchCamera {
    self.useBackCamera = !self.useBackCamera;
    videoSource.useBackCamera = !videoSource.useBackCamera;
}

#pragma mark - UTILS PATH

- (NSString *)documentPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (BOOL)createFolderIfNotExist:(NSString *)sPath
{
    NSError *error = nil;
    if (![self isPathExisted:sPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:sPath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if (error) {
            NSLog(@"error creating directory: %@", error);
            return NO;
        } else {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)isPathExisted:(NSString *)sPath
{
    BOOL isDirectory;
    if ([[NSFileManager defaultManager] fileExistsAtPath:sPath isDirectory:&isDirectory]) {
        if (isDirectory) {
            return NO;
        }
        else {
            return YES;
        }
    }
    return NO;
}

- (NSString *)filePathForRTCCallLog:(NSString *)sFileName
{
    NSString *folderPath = [NSString stringWithFormat:@"%@", [self documentPath]];
//    [self createFolderIfNotExist:folderPath];
//    if (sFileName.length == 0) {
//        return folderPath;
//    }
    NSString *retVal = [NSString stringWithFormat:@"%@/%@", folderPath, sFileName];
    return retVal;
}

- (long long)getCurrentTimeStamp
{
    NSDate *nowUTC = [NSDate date];
    NSTimeInterval ti = [nowUTC timeIntervalSince1970];
    return ti*1000;
}

- (NSString*) createDumpReport:(RTCLegacyStatsReport*) report {
    NSMutableDictionary* peerConnectionsObj = self.webrtcInternalDumpObj[@"PeerConnections"];
    NSMutableDictionary* pidObj = peerConnectionsObj[@"pid"];
    NSMutableDictionary* statsObj = pidObj[@"stats"];
    NSArray* listKeyAndChartObj = [self createKeyAndChartObj:report];
    for (NSDictionary* item in listKeyAndChartObj) {
        NSString* key = [item allKeys].count > 0 ? [item allKeys][0] : @"";
        Chart* newChart = [[Chart alloc] initWithDictionary:[item objectForKey:key]];
        Chart* c = [[Chart alloc] initWithDictionary:[self.mapChart objectForKey:key]];
        if (![self.mapChart objectForKey:key]) {
            c = newChart;
            NSMutableDictionary* dict = [self convertChartToDictionary:c];
            [self.mapChart setObject:dict forKey:key];
        } else {
            c.endTime = newChart.endTime;
            NSMutableArray* valuesArray = [[NSMutableArray alloc] initWithArray:[self convertStringToArray:c.values]];
            [valuesArray addObjectsFromArray:[self convertStringToArray:newChart.values]];
            c.values = [self convertArrayToString:valuesArray];
//            [c.values addObjectsFromArray:newChart.values];
        }
        NSLog(@"createDumpReport %@", [NSString stringWithFormat:@"%@", c.values]);
        NSMutableDictionary* dict = [self convertChartToDictionary:c];
        [statsObj removeObjectForKey:key];
        [statsObj setObject:dict forKey:key];
    }
    [pidObj removeObjectForKey:@"stats"];
    [pidObj setObject:statsObj forKey:@"stats"];
    
    [peerConnectionsObj removeObjectForKey:@"pid"];
    [peerConnectionsObj setObject:pidObj forKey:@"pid"];
    
    [self.webrtcInternalDumpObj removeObjectForKey:@"PeerConnections"];
    [self.webrtcInternalDumpObj setObject:peerConnectionsObj forKey:@"PeerConnections"];
    
    return [self getJsonStringFromNSDictionaty:self.webrtcInternalDumpObj];
}

- (NSMutableDictionary*) createDumpObject {
    NSMutableDictionary* webrtcInternalDumpObj = [[NSMutableDictionary alloc] init];
    
    // getUserMedia
    NSMutableArray* getUserMediaArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary* getUserMediaObj = [[NSMutableDictionary alloc] init];
    [getUserMediaObj setObject:@"audio" forKey:@"audio"];
    [getUserMediaObj setObject:@"origin" forKey:@"origin"];
    [getUserMediaObj setObject:@"pid" forKey:@"pid"];
    [getUserMediaObj setObject:@"rid" forKey:@"rid"];
    [getUserMediaObj setObject:@"video" forKey:@"video"];
    
    [getUserMediaArray addObject:getUserMediaObj];
    [webrtcInternalDumpObj setObject:getUserMediaArray forKey:@"getUserMedia"];
    
    // PeerConnections
    NSMutableDictionary* peerConnectionsObj = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* pidObj = [[NSMutableDictionary alloc] init];
    [pidObj setObject:@"constraints" forKey:@"constraints"];
    [pidObj setObject:@"rtcConfiguration" forKey:@"rtcConfiguration"];
        
    //stats obj
    NSMutableDictionary* statsObj = [[NSMutableDictionary alloc] init];
    [pidObj setObject:statsObj forKey:@"stats"];
    [pidObj setObject:[[NSMutableArray alloc] init] forKey:@"updateLog"];
    [pidObj setObject:@"https://appr.tc/" forKey:@"url"];
    
    [peerConnectionsObj setObject:pidObj forKey:@"pid"];
    [webrtcInternalDumpObj setObject:peerConnectionsObj forKey:@"PeerConnections"];
    // UserAgent
    [webrtcInternalDumpObj setObject:@"UserAgent" forKey:@"UserAgent"];
    
    return webrtcInternalDumpObj;
}

- (void) addSignalingStatistics:(SignalingStatisticElement*) signalingStatisticElement {
    if (self.webrtcInternalDumpObj) {
        NSMutableDictionary* peerConnectionsObj = self.webrtcInternalDumpObj[@"PeerConnections"];
        NSMutableDictionary* pidObj = peerConnectionsObj[@"pid"];
        NSMutableArray* updateLogJsonArray = pidObj[@"updateLog"];
        [updateLogJsonArray addObject:signalingStatisticElement];
        
        [pidObj removeObjectForKey:@"updateLog"];
        [pidObj setObject:updateLogJsonArray forKey:@"updateLog"];
        
        [peerConnectionsObj removeObjectForKey:@"pid"];
        [peerConnectionsObj setObject:pidObj forKey:@"pid"];
        
        [self.webrtcInternalDumpObj removeObjectForKey:@"PeerConnections"];
        [self.webrtcInternalDumpObj setObject:peerConnectionsObj forKey:@"PeerConnections"];
    }
}

- (NSArray*) createKeyAndChartObj:(RTCLegacyStatsReport*) report {
    NSMutableArray* pairList = [[NSMutableArray alloc] init];
    if ([self isNotNullNSString:report.reportId]) {
        NSDictionary* reportMap = [self getReportMap:report];
        
        NSString* mediaType = nil;
        if ([report.reportId containsString:@"ssrc"]) {
            NSString* str = [self getJsonStringFromNSDictionaty:report.values];
            if ([str containsString:@"\"mediaType\":\"video\""]) {
                NSLog(@"uploadCallReport mediaType video");
                mediaType = @"video";
            } else if ([str containsString:@"\"mediaType\":\"audio\""]) {
                NSLog(@"uploadCallReport mediaType audio");
                mediaType = @"audio";
            } else {
                mediaType = @"null"; 
            }
        } else {
            mediaType = @"null";
        }
        
        for (NSString* keyReportMap in reportMap.allKeys) {
            NSString* key = [NSString stringWithFormat:@"%@-%@", report.reportId, keyReportMap];
            
            Chart* chartObj = [[Chart alloc] init];
            NSString* str = [self convertTime:[self getDateFromLongLong:report.timestamp]];
            chartObj.startTime = str;
            chartObj.endTime = str;
            chartObj.values = [reportMap objectForKey:keyReportMap];
//            chartObj.values = @"[]";
//            [chartObj.values addObject:[reportMap objectForKey:keyReportMap]];
            chartObj.mediaType = mediaType;
            
            NSMutableDictionary* pair = [[NSMutableDictionary alloc] init];
            NSMutableDictionary* dict = [self convertChartToDictionary:chartObj];
            [pair setObject:dict forKey:key];
            [pairList addObject:pair];
        }
    }
    
    return pairList;
}

//private Map<String, String> getReportMap(StatsReport report) {
//    Map<String, String> reportMap = new HashMap<>();
//    for (StatsReport.Value value : report.values) {
//        reportMap.put(value.name, value.value);
//    }
//    return reportMap;
//}

- (NSDictionary*) getReportMap:(RTCLegacyStatsReport*) report {
    NSMutableDictionary* reportMap = [[NSMutableDictionary alloc] init];
    for (NSString* key in report.values.allKeys) {
        if ([reportMap objectForKey:key]) {
            [reportMap removeObjectForKey:key];
        }
        [reportMap setObject:[report.values objectForKey:key] forKey:key];
    }
    
    return reportMap;
}

- (BOOL)isNotNullNSString:(id)input
{
    if ([input isKindOfClass:[NSString class]]) {
        NSString *string = (NSString *)input;
        if (string.length > 0) {
            return YES;
        }
    }
    return NO;
}

-(BOOL) isNumeric:(NSString*)str
{
    NSScanner *sc = [NSScanner scannerWithString: str];
    if ( [sc scanFloat:NULL] )
    {
        return [sc isAtEnd];
    }
    return NO;
}


- (NSDate *)getDateFromLongLong:(long long)timestamp
{
    return [NSDate dateWithTimeIntervalSince1970:timestamp/1000];
}

- (NSString *)convertTime:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    return [formatter stringFromDate:date];
}

- (NSString*)getJsonStringFromNSDictionaty:(NSMutableDictionary*) dictionary {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    if (! jsonData) {
        return @"";
    } else {
        NSString* dumpObjectJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return dumpObjectJson;
    }
}

- (NSMutableDictionary*)convertChartToDictionary:(Chart*)chart{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:chart.startTime forKey:@"startTime"];
    [dict setObject:chart.endTime forKey:@"endTime"];
    [dict setObject:chart.mediaType forKey:@"mediaType"];
    [dict setObject:chart.values forKey:@"values"];
    
    return dict;
}

-(NSArray*)convertStringToArray:(NSString*)values {
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"[] "];
    NSArray *array = [[[values componentsSeparatedByCharactersInSet:characterSet]
                       componentsJoinedByString:@""]
                      componentsSeparatedByString:@","];
    return array;
}

-(NSString*)convertArrayToString:(NSArray*)array {
    NSString* values = @"";
    for (int i = 0; i < array.count; i++) {
        if (![self isNotNullNSString:values]) {
            if ([self isNumeric:array[i]]) {
                values = [NSString stringWithFormat:@"%@", array[i]];
            } else {
                values = [NSString stringWithFormat:@"\"%@\"", array[i]];
            }
            
        } else {
            if ([self isNumeric:array[i]]) {
                values = [NSString stringWithFormat:@"%@, %@", values, array[i]];
            } else {
                values = [NSString stringWithFormat:@"%@, \"%@\"", values, array[i]];
            }
        }
    }
    return [NSString stringWithFormat:@"[%@]", values];
}

@end
