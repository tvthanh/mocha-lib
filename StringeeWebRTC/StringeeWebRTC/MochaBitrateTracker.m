//
//  MochaBitrateTracker.m
//  StringeeWebRTC
//
//  Created by Duc Nguyen on 1/25/18.
//  Copyright © 2018 Dau Ngoc Huy. All rights reserved.
//

#import "MochaBitrateTracker.h"
#import <QuartzCore/QuartzCore.h>

@implementation MochaBitrateTracker {
    CFTimeInterval _prevTime;
    NSInteger _prevByteCount;
}

@synthesize bitrate = _bitrate;

+ (NSString *)bitrateStringForBitrate:(double)bitrate {
    if (bitrate > 1e6) {
        return [NSString stringWithFormat:@"%.2fMbps", bitrate * 1e-6];
    } else if (bitrate > 1e3) {
        return [NSString stringWithFormat:@"%.0fKbps", bitrate * 1e-3];
    } else {
        return [NSString stringWithFormat:@"%.0fbps", bitrate];
    }
}

- (NSString *)bitrateString {
    return [[self class] bitrateStringForBitrate:_bitrate];
}

- (void)updateBitrateWithCurrentByteCount:(NSInteger)byteCount {
    CFTimeInterval currentTime = CACurrentMediaTime();
    
//    NSLog(@"updateBitrateWithCurrentByteCount --bytecount---:%d",(int)byteCount);
//    NSLog(@"updateBitrateWithCurrentByteCount --prev-bytecount---:%d",(int)_prevByteCount);
    
    if (_prevByteCount > 0) {
        _bitrate = (byteCount - _prevByteCount) * 8 / (currentTime - _prevTime);
    }
    
//    if (byteCount > _prevByteCount) {
//        if (_prevTime == 0) {
//            _bitrate = byteCount;
//        } else {
//            _bitrate = (byteCount - _prevByteCount) * 8 / (currentTime - _prevTime);
//        }
//    }
    
//    NSLog(@"updateBitrateWithCurrentByteCount --bitrate---:%d",(int)_bitrate);
    _prevByteCount = byteCount;
    _prevTime = currentTime;
}

- (void)updateBitrateWithReceiveCurrentByteCount:(NSInteger)byteCount
{
    CFTimeInterval currentTime = CACurrentMediaTime();
//    NSLog(@"updateBitrateWithCurrentByteCount --bytecount---:%d",(int)byteCount);
//    NSLog(@"updateBitrateWithCurrentByteCount --prev-bytecount---:%d",(int)_prevByteCount);
    
    if (_prevByteCount > 0) {
        _bitrate = (byteCount - _prevByteCount) * 8 / (currentTime - _prevTime);
    } else {
        _bitrate = -1;
    }
    //    if (byteCount > _prevByteCount) {
    //        if (_prevTime == 0) {
    //            _bitrate = byteCount;
    //        } else {
    //            _bitrate = (byteCount - _prevByteCount) * 8 / (currentTime - _prevTime);
    //        }
    //    }
//    NSLog(@"updateBitrateWithCurrentByteCount --bitrate---:%d",(int)_bitrate);
    
    _prevByteCount = byteCount;
    _prevTime = currentTime;
}

@end
