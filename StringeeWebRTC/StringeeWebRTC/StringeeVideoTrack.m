//
//  StringeeVideoTrack.m
//  TestWebrtcnew
//
//  Created by Andy Dau on 9/20/16.
//  Copyright © 2016 Dau Ngoc Huy. All rights reserved.
//

#import "StringeeVideoTrack.h"

@implementation StringeeVideoTrack

- (instancetype)initWithRTCVideoTrack:(RTCVideoTrack *)rtcVideoTrack1 {
    if ((self = [super init])) {
        rtcVideoTrack = rtcVideoTrack1;
    }
    
    return self;
}

- (void)addRenderer:(id<StringeeVideoRenderer>)renderer {
    [rtcVideoTrack addRenderer:renderer];
}

- (void)removeRenderer:(id<StringeeVideoRenderer>)renderer {
    [rtcVideoTrack removeRenderer:renderer];
}

@end
